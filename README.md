
# Installation setup
- create and populate user table
```
$ php artisan migrate
$ php artisan db:seed --class UserTableSeeder 
```
- copy and pase .env.example to .env
```
 $ cp .env.example .test
```

- data base name = pokemon