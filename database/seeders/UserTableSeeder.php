<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Let's clear the users table first
       User::truncate();

       $faker = \Faker\Factory::create();
       $fakePassword = Hash::make('123456');

       User::create([
           'first_name' => 'admin',
           'last_name' => 'admin',
           'username' => 'admin',
           'birthday' => '1997-05-30',
           'password' =>  Hash::make('admin1234'),
       ]);

       // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            $firstName = $faker->firstName;
            $lastName = $faker->firstName;
            $user = User::create([
                'first_name' =>  $firstName,
                'last_name' => $lastName,
                'username' => $firstName.$lastName,
                'password' => $fakePassword,
                'birthday' => '1997-05-30'
            ]);
            $token = $user->createToken('myapptoken')->plainTextToken;
        }
    }
}
