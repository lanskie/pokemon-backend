<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MyPokemon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index() 
    {
        $users = User::all();
        foreach ($users as $user) {
            $pokemon = MyPokemon::where('user_id', $user->id)->get();
            $user->pokemon = $pokemon;
        }
        return $users;
    }

    public function getUserInfo(Request $request, $id)
    {
        $user = User::where('id',$id)->get();
        $myPokemon = MyPokemon::where('user_id', $id)->get();
        $user[0]->my_pokemon = $myPokemon;
        return $user;
    }

    public function update(Request $request) {
        $fields = $request->validate([
            'id' => 'required',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            "birthday" => 'required|string'
        ]);
        $duplicate = User::where('username', $fields['username'])->get();
        if (count($duplicate) >= 1 && $duplicate[0]->id != $fields['id']) {
            $message = 'username already taken';
            return ['error' =>  $message];
        }
        $foundUser = User::findOrFail($fields['id']);
        $foundUser->first_name = $fields['first_name'];
        $foundUser->last_name = $fields['last_name'];
        $foundUser->username = $fields['username'];
        $foundUser->birthday = $fields['birthday'];
        if (!empty($request->input('password'))) {
            $foundUser->password = bcrypt($request->input('password'));
        }
        $foundUser->save();
        $token = $foundUser->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $foundUser,
            'token' => $token
        ];

        return response($response);
    }
}
