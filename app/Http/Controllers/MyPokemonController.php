<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MyPokemon;
class MyPokemonController extends Controller
{
    public function insertLike(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            'is_like' => 'required',
            'user_id' => 'required'
        ]);
        $isExist = MyPokemon::where('user_id', $fields['user_id'])
            ->where('name', $fields['name'])
            ->get();
        if (count($isExist) >= 1) {
            $status = "You already have assigned that pokemon";
            $res = (object) array();
            $res->error = $status;
            return $res;
        }
        $count = MyPokemon::where('user_id', $fields['user_id'])
            ->where('is_like', $fields['is_like'])
            ->get();
        if (count($count) >= 3) {
            $status = $fields['is_like'] === 1 ? "Liked" : "Dislike";
            $res = (object) array();
            $res->error = "You already have 3 $status  pokemon";
            return $res;
        }
        MyPokemon::create([
            'name' => $fields['name'],
            'is_like' => $fields['is_like'],
            'user_id' => $fields['user_id'],
        ]);
        return MyPokemon::where('user_id', $fields['user_id'])->get();
    }

    public function deleteMyPokemon($id)
    {
        $findPokemon = MyPokemon::where('id', $id)->get();
        $userId = $findPokemon[0]->user_id;
        MyPokemon::where('id', $id)->delete();
        return MyPokemon::where('user_id', $userId)->get();
    }
}
