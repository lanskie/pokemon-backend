<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasFactory, HasApiTokens;
    protected $table = 'users';

    protected $fillable = ['first_name', 'last_name', 'birthday', "password", "username", "birthday"];

    protected $hidden = [
        'password',
        'remember_token',
    ];
}
